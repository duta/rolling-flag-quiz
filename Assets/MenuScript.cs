﻿using GoogleMobileAds.Api;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {

    [System.Serializable]
    public class CountryList
    {
        public List<CountryData> List;
    }
    [System.Serializable]
    public struct CountryData
    {
        public string Name;
        public string A2Code;
        public string A3Code;
        public string NumCode;
        public string ISOcode;
        public string Independent;
    }

    private string countryjson = "{\"List\":[{\"Name\":\"Afghanistan\",\"A2Code\":\"AF\",\"A3Code\":\"AFG\",\"NumCode\":\"004\",\"ISOcode\":\"ISO 3166-2:AF\",\"Independent\":\"Yes\"},{\"Name\":\"Albania\",\"A2Code\":\"AL\",\"A3Code\":\"ALB\",\"NumCode\":\"008\",\"ISOcode\":\"ISO 3166-2:AL\",\"Independent\":\"Yes\"},{\"Name\":\"Algeria\",\"A2Code\":\"DZ\",\"A3Code\":\"DZA\",\"NumCode\":\"012\",\"ISOcode\":\"ISO 3166-2:DZ\",\"Independent\":\"Yes\"},{\"Name\":\"Andorra\",\"A2Code\":\"AD\",\"A3Code\":\"AND\",\"NumCode\":\"020\",\"ISOcode\":\"ISO 3166-2:AD\",\"Independent\":\"Yes\"},{\"Name\":\"Angola\",\"A2Code\":\"AO\",\"A3Code\":\"AGO\",\"NumCode\":\"024\",\"ISOcode\":\"ISO 3166-2:AO\",\"Independent\":\"Yes\"},{\"Name\":\"Antigua and Barbuda\",\"A2Code\":\"AG\",\"A3Code\":\"ATG\",\"NumCode\":\"028\",\"ISOcode\":\"ISO 3166-2:AG\",\"Independent\":\"Yes\"},{\"Name\":\"Argentina\",\"A2Code\":\"AR\",\"A3Code\":\"ARG\",\"NumCode\":\"032\",\"ISOcode\":\"ISO 3166-2:AR\",\"Independent\":\"Yes\"},{\"Name\":\"Armenia\",\"A2Code\":\"AM\",\"A3Code\":\"ARM\",\"NumCode\":\"051\",\"ISOcode\":\"ISO 3166-2:AM\",\"Independent\":\"Yes\"},{\"Name\":\"Australia\",\"A2Code\":\"AU\",\"A3Code\":\"AUS\",\"NumCode\":\"036\",\"ISOcode\":\"ISO 3166-2:AU\",\"Independent\":\"Yes\"},{\"Name\":\"Austria\",\"A2Code\":\"AT\",\"A3Code\":\"AUT\",\"NumCode\":\"040\",\"ISOcode\":\"ISO 3166-2:AT\",\"Independent\":\"Yes\"},{\"Name\":\"Azerbaijan\",\"A2Code\":\"AZ\",\"A3Code\":\"AZE\",\"NumCode\":\"031\",\"ISOcode\":\"ISO 3166-2:AZ\",\"Independent\":\"Yes\"},{\"Name\":\"The Bahamas\",\"A2Code\":\"BS\",\"A3Code\":\"BHS\",\"NumCode\":\"044\",\"ISOcode\":\"ISO 3166-2:BS\",\"Independent\":\"Yes\"},{\"Name\":\"Bahrain\",\"A2Code\":\"BH\",\"A3Code\":\"BHR\",\"NumCode\":\"048\",\"ISOcode\":\"ISO 3166-2:BH\",\"Independent\":\"Yes\"},{\"Name\":\"Bangladesh\",\"A2Code\":\"BD\",\"A3Code\":\"BGD\",\"NumCode\":\"050\",\"ISOcode\":\"ISO 3166-2:BD\",\"Independent\":\"Yes\"},{\"Name\":\"Barbados\",\"A2Code\":\"BB\",\"A3Code\":\"BRB\",\"NumCode\":\"052\",\"ISOcode\":\"ISO 3166-2:BB\",\"Independent\":\"Yes\"},{\"Name\":\"Belarus\",\"A2Code\":\"BY\",\"A3Code\":\"BLR\",\"NumCode\":\"112\",\"ISOcode\":\"ISO 3166-2:BY\",\"Independent\":\"Yes\"},{\"Name\":\"Belgium\",\"A2Code\":\"BE\",\"A3Code\":\"BEL\",\"NumCode\":\"056\",\"ISOcode\":\"ISO 3166-2:BE\",\"Independent\":\"Yes\"},{\"Name\":\"Belize\",\"A2Code\":\"BZ\",\"A3Code\":\"BLZ\",\"NumCode\":\"084\",\"ISOcode\":\"ISO 3166-2:BZ\",\"Independent\":\"Yes\"},{\"Name\":\"Benin\",\"A2Code\":\"BJ\",\"A3Code\":\"BEN\",\"NumCode\":\"204\",\"ISOcode\":\"ISO 3166-2:BJ\",\"Independent\":\"Yes\"},{\"Name\":\"Bhutan\",\"A2Code\":\"BT\",\"A3Code\":\"BTN\",\"NumCode\":\"064\",\"ISOcode\":\"ISO 3166-2:BT\",\"Independent\":\"Yes\"},{\"Name\":\"Bolivia\",\"A2Code\":\"BO\",\"A3Code\":\"BOL\",\"NumCode\":\"068\",\"ISOcode\":\"ISO 3166-2:BO\",\"Independent\":\"Yes\"},{\"Name\":\"Bosnia and Herzegovina\",\"A2Code\":\"BA\",\"A3Code\":\"BIH\",\"NumCode\":\"070\",\"ISOcode\":\"ISO 3166-2:BA\",\"Independent\":\"Yes\"},{\"Name\":\"Botswana\",\"A2Code\":\"BW\",\"A3Code\":\"BWA\",\"NumCode\":\"072\",\"ISOcode\":\"ISO 3166-2:BW\",\"Independent\":\"Yes\"},{\"Name\":\"Brazil\",\"A2Code\":\"BR\",\"A3Code\":\"BRA\",\"NumCode\":\"076\",\"ISOcode\":\"ISO 3166-2:BR\",\"Independent\":\"Yes\"},{\"Name\":\"Brunei Darussalam\",\"A2Code\":\"BN\",\"A3Code\":\"BRN\",\"NumCode\":\"096\",\"ISOcode\":\"ISO 3166-2:BN\",\"Independent\":\"Yes\"},{\"Name\":\"Bulgaria\",\"A2Code\":\"BG\",\"A3Code\":\"BGR\",\"NumCode\":\"100\",\"ISOcode\":\"ISO 3166-2:BG\",\"Independent\":\"Yes\"},{\"Name\":\"Burkina Faso\",\"A2Code\":\"BF\",\"A3Code\":\"BFA\",\"NumCode\":\"854\",\"ISOcode\":\"ISO 3166-2:BF\",\"Independent\":\"Yes\"},{\"Name\":\"Burundi\",\"A2Code\":\"BI\",\"A3Code\":\"BDI\",\"NumCode\":\"108\",\"ISOcode\":\"ISO 3166-2:BI\",\"Independent\":\"Yes\"},{\"Name\":\"Cape Verde\",\"A2Code\":\"CV\",\"A3Code\":\"CPV\",\"NumCode\":\"132\",\"ISOcode\":\"ISO 3166-2:CV\",\"Independent\":\"Yes\"},{\"Name\":\"Cambodia\",\"A2Code\":\"KH\",\"A3Code\":\"KHM\",\"NumCode\":\"116\",\"ISOcode\":\"ISO 3166-2:KH\",\"Independent\":\"Yes\"},{\"Name\":\"Cameroon\",\"A2Code\":\"CM\",\"A3Code\":\"CMR\",\"NumCode\":\"120\",\"ISOcode\":\"ISO 3166-2:CM\",\"Independent\":\"Yes\"},{\"Name\":\"Canada\",\"A2Code\":\"CA\",\"A3Code\":\"CAN\",\"NumCode\":\"124\",\"ISOcode\":\"ISO 3166-2:CA\",\"Independent\":\"Yes\"},{\"Name\":\"Central African Republic\",\"A2Code\":\"CF\",\"A3Code\":\"CAF\",\"NumCode\":\"140\",\"ISOcode\":\"ISO 3166-2:CF\",\"Independent\":\"Yes\"},{\"Name\":\"Chad\",\"A2Code\":\"TD\",\"A3Code\":\"TCD\",\"NumCode\":\"148\",\"ISOcode\":\"ISO 3166-2:TD\",\"Independent\":\"Yes\"},{\"Name\":\"Chile\",\"A2Code\":\"CL\",\"A3Code\":\"CHL\",\"NumCode\":\"152\",\"ISOcode\":\"ISO 3166-2:CL\",\"Independent\":\"Yes\"},{\"Name\":\"China\",\"A2Code\":\"CN\",\"A3Code\":\"CHN\",\"NumCode\":\"156\",\"ISOcode\":\"ISO 3166-2:CN\",\"Independent\":\"Yes\"},{\"Name\":\"Colombia\",\"A2Code\":\"CO\",\"A3Code\":\"COL\",\"NumCode\":\"170\",\"ISOcode\":\"ISO 3166-2:CO\",\"Independent\":\"Yes\"},{\"Name\":\"Comoros\",\"A2Code\":\"KM\",\"A3Code\":\"COM\",\"NumCode\":\"174\",\"ISOcode\":\"ISO 3166-2:KM\",\"Independent\":\"Yes\"},{\"Name\":\"Republic of the Congo\",\"A2Code\":\"CG\",\"A3Code\":\"COG\",\"NumCode\":\"178\",\"ISOcode\":\"ISO 3166-2:CG\",\"Independent\":\"Yes\"},{\"Name\":\"Democratic Republic of the Congo\",\"A2Code\":\"CD\",\"A3Code\":\"COD\",\"NumCode\":\"180\",\"ISOcode\":\"ISO 3166-2:CD\",\"Independent\":\"Yes\"},{\"Name\":\"Costa Rica\",\"A2Code\":\"CR\",\"A3Code\":\"CRI\",\"NumCode\":\"188\",\"ISOcode\":\"ISO 3166-2:CR\",\"Independent\":\"Yes\"},{\"Name\":\"Ivory Coast\",\"A2Code\":\"CI\",\"A3Code\":\"CIV\",\"NumCode\":\"384\",\"ISOcode\":\"ISO 3166-2:CI\",\"Independent\":\"Yes\"},{\"Name\":\"Croatia\",\"A2Code\":\"HR\",\"A3Code\":\"HRV\",\"NumCode\":\"191\",\"ISOcode\":\"ISO 3166-2:HR\",\"Independent\":\"Yes\"},{\"Name\":\"Cuba\",\"A2Code\":\"CU\",\"A3Code\":\"CUB\",\"NumCode\":\"192\",\"ISOcode\":\"ISO 3166-2:CU\",\"Independent\":\"Yes\"},{\"Name\":\"Cyprus\",\"A2Code\":\"CY\",\"A3Code\":\"CYP\",\"NumCode\":\"196\",\"ISOcode\":\"ISO 3166-2:CY\",\"Independent\":\"Yes\"},{\"Name\":\"Czech\",\"A2Code\":\"CZ\",\"A3Code\":\"CZE\",\"NumCode\":\"203\",\"ISOcode\":\"ISO 3166-2:CZ\",\"Independent\":\"Yes\"},{\"Name\":\"Denmark\",\"A2Code\":\"DK\",\"A3Code\":\"DNK\",\"NumCode\":\"208\",\"ISOcode\":\"ISO 3166-2:DK\",\"Independent\":\"Yes\"},{\"Name\":\"Djibouti\",\"A2Code\":\"DJ\",\"A3Code\":\"DJI\",\"NumCode\":\"262\",\"ISOcode\":\"ISO 3166-2:DJ\",\"Independent\":\"Yes\"},{\"Name\":\"Dominica\",\"A2Code\":\"DM\",\"A3Code\":\"DMA\",\"NumCode\":\"212\",\"ISOcode\":\"ISO 3166-2:DM\",\"Independent\":\"Yes\"},{\"Name\":\"Dominican Republic\",\"A2Code\":\"DO\",\"A3Code\":\"DOM\",\"NumCode\":\"214\",\"ISOcode\":\"ISO 3166-2:DO\",\"Independent\":\"Yes\"},{\"Name\":\"Ecuador\",\"A2Code\":\"EC\",\"A3Code\":\"ECU\",\"NumCode\":\"218\",\"ISOcode\":\"ISO 3166-2:EC\",\"Independent\":\"Yes\"},{\"Name\":\"Egypt\",\"A2Code\":\"EG\",\"A3Code\":\"EGY\",\"NumCode\":\"818\",\"ISOcode\":\"ISO 3166-2:EG\",\"Independent\":\"Yes\"},{\"Name\":\"El Salvador\",\"A2Code\":\"SV\",\"A3Code\":\"SLV\",\"NumCode\":\"222\",\"ISOcode\":\"ISO 3166-2:SV\",\"Independent\":\"Yes\"},{\"Name\":\"Equatorial Guinea\",\"A2Code\":\"GQ\",\"A3Code\":\"GNQ\",\"NumCode\":\"226\",\"ISOcode\":\"ISO 3166-2:GQ\",\"Independent\":\"Yes\"},{\"Name\":\"Eritrea\",\"A2Code\":\"ER\",\"A3Code\":\"ERI\",\"NumCode\":\"232\",\"ISOcode\":\"ISO 3166-2:ER\",\"Independent\":\"Yes\"},{\"Name\":\"Estonia\",\"A2Code\":\"EE\",\"A3Code\":\"EST\",\"NumCode\":\"233\",\"ISOcode\":\"ISO 3166-2:EE\",\"Independent\":\"Yes\"},{\"Name\":\"Swaziland Eswatini\",\"A2Code\":\"SZ\",\"A3Code\":\"SWZ\",\"NumCode\":\"748\",\"ISOcode\":\"ISO 3166-2:SZ\",\"Independent\":\"Yes\"},{\"Name\":\"Ethiopia\",\"A2Code\":\"ET\",\"A3Code\":\"ETH\",\"NumCode\":\"231\",\"ISOcode\":\"ISO 3166-2:ET\",\"Independent\":\"Yes\"},{\"Name\":\"Fiji\",\"A2Code\":\"FJ\",\"A3Code\":\"FJI\",\"NumCode\":\"242\",\"ISOcode\":\"ISO 3166-2:FJ\",\"Independent\":\"Yes\"},{\"Name\":\"Finland\",\"A2Code\":\"FI\",\"A3Code\":\"FIN\",\"NumCode\":\"246\",\"ISOcode\":\"ISO 3166-2:FI\",\"Independent\":\"Yes\"},{\"Name\":\"France\",\"A2Code\":\"FR\",\"A3Code\":\"FRA\",\"NumCode\":\"250\",\"ISOcode\":\"ISO 3166-2:FR\",\"Independent\":\"Yes\"},{\"Name\":\"Gabon\",\"A2Code\":\"GA\",\"A3Code\":\"GAB\",\"NumCode\":\"266\",\"ISOcode\":\"ISO 3166-2:GA\",\"Independent\":\"Yes\"},{\"Name\":\"The Gambia\",\"A2Code\":\"GM\",\"A3Code\":\"GMB\",\"NumCode\":\"270\",\"ISOcode\":\"ISO 3166-2:GM\",\"Independent\":\"Yes\"},{\"Name\":\"Georgia\",\"A2Code\":\"GE\",\"A3Code\":\"GEO\",\"NumCode\":\"268\",\"ISOcode\":\"ISO 3166-2:GE\",\"Independent\":\"Yes\"},{\"Name\":\"Germany\",\"A2Code\":\"DE\",\"A3Code\":\"DEU\",\"NumCode\":\"276\",\"ISOcode\":\"ISO 3166-2:DE\",\"Independent\":\"Yes\"},{\"Name\":\"Ghana\",\"A2Code\":\"GH\",\"A3Code\":\"GHA\",\"NumCode\":\"288\",\"ISOcode\":\"ISO 3166-2:GH\",\"Independent\":\"Yes\"},{\"Name\":\"Greece\",\"A2Code\":\"GR\",\"A3Code\":\"GRC\",\"NumCode\":\"300\",\"ISOcode\":\"ISO 3166-2:GR\",\"Independent\":\"Yes\"},{\"Name\":\"Grenada\",\"A2Code\":\"GD\",\"A3Code\":\"GRD\",\"NumCode\":\"308\",\"ISOcode\":\"ISO 3166-2:GD\",\"Independent\":\"Yes\"},{\"Name\":\"Guatemala\",\"A2Code\":\"GT\",\"A3Code\":\"GTM\",\"NumCode\":\"320\",\"ISOcode\":\"ISO 3166-2:GT\",\"Independent\":\"Yes\"},{\"Name\":\"Guinea\",\"A2Code\":\"GN\",\"A3Code\":\"GIN\",\"NumCode\":\"324\",\"ISOcode\":\"ISO 3166-2:GN\",\"Independent\":\"Yes\"},{\"Name\":\"Guinea-Bissau\",\"A2Code\":\"GW\",\"A3Code\":\"GNB\",\"NumCode\":\"624\",\"ISOcode\":\"ISO 3166-2:GW\",\"Independent\":\"Yes\"},{\"Name\":\"Guyana\",\"A2Code\":\"GY\",\"A3Code\":\"GUY\",\"NumCode\":\"328\",\"ISOcode\":\"ISO 3166-2:GY\",\"Independent\":\"Yes\"},{\"Name\":\"Haiti\",\"A2Code\":\"HT\",\"A3Code\":\"HTI\",\"NumCode\":\"332\",\"ISOcode\":\"ISO 3166-2:HT\",\"Independent\":\"Yes\"},{\"Name\":\"Vatican City\",\"A2Code\":\"VA\",\"A3Code\":\"VAT\",\"NumCode\":\"336\",\"ISOcode\":\"ISO 3166-2:VA\",\"Independent\":\"Yes\"},{\"Name\":\"Honduras\",\"A2Code\":\"HN\",\"A3Code\":\"HND\",\"NumCode\":\"340\",\"ISOcode\":\"ISO 3166-2:HN\",\"Independent\":\"Yes\"},{\"Name\":\"Hungary\",\"A2Code\":\"HU\",\"A3Code\":\"HUN\",\"NumCode\":\"348\",\"ISOcode\":\"ISO 3166-2:HU\",\"Independent\":\"Yes\"},{\"Name\":\"Iceland\",\"A2Code\":\"IS\",\"A3Code\":\"ISL\",\"NumCode\":\"352\",\"ISOcode\":\"ISO 3166-2:IS\",\"Independent\":\"Yes\"},{\"Name\":\"India\",\"A2Code\":\"IN\",\"A3Code\":\"IND\",\"NumCode\":\"356\",\"ISOcode\":\"ISO 3166-2:IN\",\"Independent\":\"Yes\"},{\"Name\":\"Indonesia\",\"A2Code\":\"ID\",\"A3Code\":\"IDN\",\"NumCode\":\"360\",\"ISOcode\":\"ISO 3166-2:ID\",\"Independent\":\"Yes\"},{\"Name\":\"Iran\",\"A2Code\":\"IR\",\"A3Code\":\"IRN\",\"NumCode\":\"364\",\"ISOcode\":\"ISO 3166-2:IR\",\"Independent\":\"Yes\"},{\"Name\":\"Iraq\",\"A2Code\":\"IQ\",\"A3Code\":\"IRQ\",\"NumCode\":\"368\",\"ISOcode\":\"ISO 3166-2:IQ\",\"Independent\":\"Yes\"},{\"Name\":\"Ireland\",\"A2Code\":\"IE\",\"A3Code\":\"IRL\",\"NumCode\":\"372\",\"ISOcode\":\"ISO 3166-2:IE\",\"Independent\":\"Yes\"},{\"Name\":\"Israel\",\"A2Code\":\"IL\",\"A3Code\":\"ISR\",\"NumCode\":\"376\",\"ISOcode\":\"ISO 3166-2:IL\",\"Independent\":\"Yes\"},{\"Name\":\"Italy\",\"A2Code\":\"IT\",\"A3Code\":\"ITA\",\"NumCode\":\"380\",\"ISOcode\":\"ISO 3166-2:IT\",\"Independent\":\"Yes\"},{\"Name\":\"Jamaica\",\"A2Code\":\"JM\",\"A3Code\":\"JAM\",\"NumCode\":\"388\",\"ISOcode\":\"ISO 3166-2:JM\",\"Independent\":\"Yes\"},{\"Name\":\"Japan\",\"A2Code\":\"JP\",\"A3Code\":\"JPN\",\"NumCode\":\"392\",\"ISOcode\":\"ISO 3166-2:JP\",\"Independent\":\"Yes\"},{\"Name\":\"Jordan\",\"A2Code\":\"JO\",\"A3Code\":\"JOR\",\"NumCode\":\"400\",\"ISOcode\":\"ISO 3166-2:JO\",\"Independent\":\"Yes\"},{\"Name\":\"Kazakhstan\",\"A2Code\":\"KZ\",\"A3Code\":\"KAZ\",\"NumCode\":\"398\",\"ISOcode\":\"ISO 3166-2:KZ\",\"Independent\":\"Yes\"},{\"Name\":\"Kenya\",\"A2Code\":\"KE\",\"A3Code\":\"KEN\",\"NumCode\":\"404\",\"ISOcode\":\"ISO 3166-2:KE\",\"Independent\":\"Yes\"},{\"Name\":\"Kiribati\",\"A2Code\":\"KI\",\"A3Code\":\"KIR\",\"NumCode\":\"296\",\"ISOcode\":\"ISO 3166-2:KI\",\"Independent\":\"Yes\"},{\"Name\":\"North Korea\",\"A2Code\":\"KP\",\"A3Code\":\"PRK\",\"NumCode\":\"408\",\"ISOcode\":\"ISO 3166-2:KP\",\"Independent\":\"Yes\"},{\"Name\":\"South Korea\",\"A2Code\":\"KR\",\"A3Code\":\"KOR\",\"NumCode\":\"410\",\"ISOcode\":\"ISO 3166-2:KR\",\"Independent\":\"Yes\"},{\"Name\":\"Kuwait\",\"A2Code\":\"KW\",\"A3Code\":\"KWT\",\"NumCode\":\"414\",\"ISOcode\":\"ISO 3166-2:KW\",\"Independent\":\"Yes\"},{\"Name\":\"Kyrgyzstan\",\"A2Code\":\"KG\",\"A3Code\":\"KGZ\",\"NumCode\":\"417\",\"ISOcode\":\"ISO 3166-2:KG\",\"Independent\":\"Yes\"},{\"Name\":\"Laos\",\"A2Code\":\"LA\",\"A3Code\":\"LAO\",\"NumCode\":\"418\",\"ISOcode\":\"ISO 3166-2:LA\",\"Independent\":\"Yes\"},{\"Name\":\"Latvia\",\"A2Code\":\"LV\",\"A3Code\":\"LVA\",\"NumCode\":\"428\",\"ISOcode\":\"ISO 3166-2:LV\",\"Independent\":\"Yes\"},{\"Name\":\"Lebanon\",\"A2Code\":\"LB\",\"A3Code\":\"LBN\",\"NumCode\":\"422\",\"ISOcode\":\"ISO 3166-2:LB\",\"Independent\":\"Yes\"},{\"Name\":\"Lesotho\",\"A2Code\":\"LS\",\"A3Code\":\"LSO\",\"NumCode\":\"426\",\"ISOcode\":\"ISO 3166-2:LS\",\"Independent\":\"Yes\"},{\"Name\":\"Liberia\",\"A2Code\":\"LR\",\"A3Code\":\"LBR\",\"NumCode\":\"430\",\"ISOcode\":\"ISO 3166-2:LR\",\"Independent\":\"Yes\"},{\"Name\":\"Libya\",\"A2Code\":\"LY\",\"A3Code\":\"LBY\",\"NumCode\":\"434\",\"ISOcode\":\"ISO 3166-2:LY\",\"Independent\":\"Yes\"},{\"Name\":\"Liechtenstein\",\"A2Code\":\"LI\",\"A3Code\":\"LIE\",\"NumCode\":\"438\",\"ISOcode\":\"ISO 3166-2:LI\",\"Independent\":\"Yes\"},{\"Name\":\"Lithuania\",\"A2Code\":\"LT\",\"A3Code\":\"LTU\",\"NumCode\":\"440\",\"ISOcode\":\"ISO 3166-2:LT\",\"Independent\":\"Yes\"},{\"Name\":\"Luxembourg\",\"A2Code\":\"LU\",\"A3Code\":\"LUX\",\"NumCode\":\"442\",\"ISOcode\":\"ISO 3166-2:LU\",\"Independent\":\"Yes\"},{\"Name\":\"Republic of Macedonia\",\"A2Code\":\"MK\",\"A3Code\":\"MKD\",\"NumCode\":\"807\",\"ISOcode\":\"ISO 3166-2:MK\",\"Independent\":\"Yes\"},{\"Name\":\"Madagascar\",\"A2Code\":\"MG\",\"A3Code\":\"MDG\",\"NumCode\":\"450\",\"ISOcode\":\"ISO 3166-2:MG\",\"Independent\":\"Yes\"},{\"Name\":\"Malawi\",\"A2Code\":\"MW\",\"A3Code\":\"MWI\",\"NumCode\":\"454\",\"ISOcode\":\"ISO 3166-2:MW\",\"Independent\":\"Yes\"},{\"Name\":\"Malaysia\",\"A2Code\":\"MY\",\"A3Code\":\"MYS\",\"NumCode\":\"458\",\"ISOcode\":\"ISO 3166-2:MY\",\"Independent\":\"Yes\"},{\"Name\":\"Maldives\",\"A2Code\":\"MV\",\"A3Code\":\"MDV\",\"NumCode\":\"462\",\"ISOcode\":\"ISO 3166-2:MV\",\"Independent\":\"Yes\"},{\"Name\":\"Mali\",\"A2Code\":\"ML\",\"A3Code\":\"MLI\",\"NumCode\":\"466\",\"ISOcode\":\"ISO 3166-2:ML\",\"Independent\":\"Yes\"},{\"Name\":\"Malta\",\"A2Code\":\"MT\",\"A3Code\":\"MLT\",\"NumCode\":\"470\",\"ISOcode\":\"ISO 3166-2:MT\",\"Independent\":\"Yes\"},{\"Name\":\"Marshall Islands\",\"A2Code\":\"MH\",\"A3Code\":\"MHL\",\"NumCode\":\"584\",\"ISOcode\":\"ISO 3166-2:MH\",\"Independent\":\"Yes\"},{\"Name\":\"Mauritania\",\"A2Code\":\"MR\",\"A3Code\":\"MRT\",\"NumCode\":\"478\",\"ISOcode\":\"ISO 3166-2:MR\",\"Independent\":\"Yes\"},{\"Name\":\"Mauritius\",\"A2Code\":\"MU\",\"A3Code\":\"MUS\",\"NumCode\":\"480\",\"ISOcode\":\"ISO 3166-2:MU\",\"Independent\":\"Yes\"},{\"Name\":\"Mexico\",\"A2Code\":\"MX\",\"A3Code\":\"MEX\",\"NumCode\":\"484\",\"ISOcode\":\"ISO 3166-2:MX\",\"Independent\":\"Yes\"},{\"Name\":\"Federated States of Micronesia\",\"A2Code\":\"FM\",\"A3Code\":\"FSM\",\"NumCode\":\"583\",\"ISOcode\":\"ISO 3166-2:FM\",\"Independent\":\"Yes\"},{\"Name\":\"Moldova\",\"A2Code\":\"MD\",\"A3Code\":\"MDA\",\"NumCode\":\"498\",\"ISOcode\":\"ISO 3166-2:MD\",\"Independent\":\"Yes\"},{\"Name\":\"Monaco\",\"A2Code\":\"MC\",\"A3Code\":\"MCO\",\"NumCode\":\"492\",\"ISOcode\":\"ISO 3166-2:MC\",\"Independent\":\"Yes\"},{\"Name\":\"Mongolia\",\"A2Code\":\"MN\",\"A3Code\":\"MNG\",\"NumCode\":\"496\",\"ISOcode\":\"ISO 3166-2:MN\",\"Independent\":\"Yes\"},{\"Name\":\"Montenegro\",\"A2Code\":\"ME\",\"A3Code\":\"MNE\",\"NumCode\":\"499\",\"ISOcode\":\"ISO 3166-2:ME\",\"Independent\":\"Yes\"},{\"Name\":\"Morocco\",\"A2Code\":\"MA\",\"A3Code\":\"MAR\",\"NumCode\":\"504\",\"ISOcode\":\"ISO 3166-2:MA\",\"Independent\":\"Yes\"},{\"Name\":\"Mozambique\",\"A2Code\":\"MZ\",\"A3Code\":\"MOZ\",\"NumCode\":\"508\",\"ISOcode\":\"ISO 3166-2:MZ\",\"Independent\":\"Yes\"},{\"Name\":\"Myanmar\",\"A2Code\":\"MM\",\"A3Code\":\"MMR\",\"NumCode\":\"104\",\"ISOcode\":\"ISO 3166-2:MM\",\"Independent\":\"Yes\"},{\"Name\":\"Namibia\",\"A2Code\":\"NA\",\"A3Code\":\"NAM\",\"NumCode\":\"516\",\"ISOcode\":\"ISO 3166-2:NA\",\"Independent\":\"Yes\"},{\"Name\":\"Nauru\",\"A2Code\":\"NR\",\"A3Code\":\"NRU\",\"NumCode\":\"520\",\"ISOcode\":\"ISO 3166-2:NR\",\"Independent\":\"Yes\"},{\"Name\":\"Nepal\",\"A2Code\":\"NP\",\"A3Code\":\"NPL\",\"NumCode\":\"524\",\"ISOcode\":\"ISO 3166-2:NP\",\"Independent\":\"Yes\"},{\"Name\":\"Netherlands\",\"A2Code\":\"NL\",\"A3Code\":\"NLD\",\"NumCode\":\"528\",\"ISOcode\":\"ISO 3166-2:NL\",\"Independent\":\"Yes\"},{\"Name\":\"New Zealand\",\"A2Code\":\"NZ\",\"A3Code\":\"NZL\",\"NumCode\":\"554\",\"ISOcode\":\"ISO 3166-2:NZ\",\"Independent\":\"Yes\"},{\"Name\":\"Nicaragua\",\"A2Code\":\"NI\",\"A3Code\":\"NIC\",\"NumCode\":\"558\",\"ISOcode\":\"ISO 3166-2:NI\",\"Independent\":\"Yes\"},{\"Name\":\"Niger\",\"A2Code\":\"NE\",\"A3Code\":\"NER\",\"NumCode\":\"562\",\"ISOcode\":\"ISO 3166-2:NE\",\"Independent\":\"Yes\"},{\"Name\":\"Nigeria\",\"A2Code\":\"NG\",\"A3Code\":\"NGA\",\"NumCode\":\"566\",\"ISOcode\":\"ISO 3166-2:NG\",\"Independent\":\"Yes\"},{\"Name\":\"Norway\",\"A2Code\":\"NO\",\"A3Code\":\"NOR\",\"NumCode\":\"578\",\"ISOcode\":\"ISO 3166-2:NO\",\"Independent\":\"Yes\"},{\"Name\":\"Oman\",\"A2Code\":\"OM\",\"A3Code\":\"OMN\",\"NumCode\":\"512\",\"ISOcode\":\"ISO 3166-2:OM\",\"Independent\":\"Yes\"},{\"Name\":\"Pakistan\",\"A2Code\":\"PK\",\"A3Code\":\"PAK\",\"NumCode\":\"586\",\"ISOcode\":\"ISO 3166-2:PK\",\"Independent\":\"Yes\"},{\"Name\":\"Palau\",\"A2Code\":\"PW\",\"A3Code\":\"PLW\",\"NumCode\":\"585\",\"ISOcode\":\"ISO 3166-2:PW\",\"Independent\":\"Yes\"},{\"Name\":\"Panama\",\"A2Code\":\"PA\",\"A3Code\":\"PAN\",\"NumCode\":\"591\",\"ISOcode\":\"ISO 3166-2:PA\",\"Independent\":\"Yes\"},{\"Name\":\"Papua New Guinea\",\"A2Code\":\"PG\",\"A3Code\":\"PNG\",\"NumCode\":\"598\",\"ISOcode\":\"ISO 3166-2:PG\",\"Independent\":\"Yes\"},{\"Name\":\"Paraguay\",\"A2Code\":\"PY\",\"A3Code\":\"PRY\",\"NumCode\":\"600\",\"ISOcode\":\"ISO 3166-2:PY\",\"Independent\":\"Yes\"},{\"Name\":\"Peru\",\"A2Code\":\"PE\",\"A3Code\":\"PER\",\"NumCode\":\"604\",\"ISOcode\":\"ISO 3166-2:PE\",\"Independent\":\"Yes\"},{\"Name\":\"Philippines\",\"A2Code\":\"PH\",\"A3Code\":\"PHL\",\"NumCode\":\"608\",\"ISOcode\":\"ISO 3166-2:PH\",\"Independent\":\"Yes\"},{\"Name\":\"Poland\",\"A2Code\":\"PL\",\"A3Code\":\"POL\",\"NumCode\":\"616\",\"ISOcode\":\"ISO 3166-2:PL\",\"Independent\":\"Yes\"},{\"Name\":\"Portugal\",\"A2Code\":\"PT\",\"A3Code\":\"PRT\",\"NumCode\":\"620\",\"ISOcode\":\"ISO 3166-2:PT\",\"Independent\":\"Yes\"},{\"Name\":\"Qatar\",\"A2Code\":\"QA\",\"A3Code\":\"QAT\",\"NumCode\":\"634\",\"ISOcode\":\"ISO 3166-2:QA\",\"Independent\":\"Yes\"},{\"Name\":\"Romania\",\"A2Code\":\"RO\",\"A3Code\":\"ROU\",\"NumCode\":\"642\",\"ISOcode\":\"ISO 3166-2:RO\",\"Independent\":\"Yes\"},{\"Name\":\"Russia\",\"A2Code\":\"RU\",\"A3Code\":\"RUS\",\"NumCode\":\"643\",\"ISOcode\":\"ISO 3166-2:RU\",\"Independent\":\"Yes\"},{\"Name\":\"Rwanda\",\"A2Code\":\"RW\",\"A3Code\":\"RWA\",\"NumCode\":\"646\",\"ISOcode\":\"ISO 3166-2:RW\",\"Independent\":\"Yes\"},{\"Name\":\"Saint Kitts and Nevis\",\"A2Code\":\"KN\",\"A3Code\":\"KNA\",\"NumCode\":\"659\",\"ISOcode\":\"ISO 3166-2:KN\",\"Independent\":\"Yes\"},{\"Name\":\"Saint Lucia\",\"A2Code\":\"LC\",\"A3Code\":\"LCA\",\"NumCode\":\"662\",\"ISOcode\":\"ISO 3166-2:LC\",\"Independent\":\"Yes\"},{\"Name\":\"Saint Vincent and the Grenadines\",\"A2Code\":\"VC\",\"A3Code\":\"VCT\",\"NumCode\":\"670\",\"ISOcode\":\"ISO 3166-2:VC\",\"Independent\":\"Yes\"},{\"Name\":\"Samoa\",\"A2Code\":\"WS\",\"A3Code\":\"WSM\",\"NumCode\":\"882\",\"ISOcode\":\"ISO 3166-2:WS\",\"Independent\":\"Yes\"},{\"Name\":\"San Marino\",\"A2Code\":\"SM\",\"A3Code\":\"SMR\",\"NumCode\":\"674\",\"ISOcode\":\"ISO 3166-2:SM\",\"Independent\":\"Yes\"},{\"Name\":\"S\u00E3o Tom\u00E9 and Pr\u00EDncipe\",\"A2Code\":\"ST\",\"A3Code\":\"STP\",\"NumCode\":\"678\",\"ISOcode\":\"ISO 3166-2:ST\",\"Independent\":\"Yes\"},{\"Name\":\"Saudi Arabia\",\"A2Code\":\"SA\",\"A3Code\":\"SAU\",\"NumCode\":\"682\",\"ISOcode\":\"ISO 3166-2:SA\",\"Independent\":\"Yes\"},{\"Name\":\"Senegal\",\"A2Code\":\"SN\",\"A3Code\":\"SEN\",\"NumCode\":\"686\",\"ISOcode\":\"ISO 3166-2:SN\",\"Independent\":\"Yes\"},{\"Name\":\"Serbia\",\"A2Code\":\"RS\",\"A3Code\":\"SRB\",\"NumCode\":\"688\",\"ISOcode\":\"ISO 3166-2:RS\",\"Independent\":\"Yes\"},{\"Name\":\"Seychelles\",\"A2Code\":\"SC\",\"A3Code\":\"SYC\",\"NumCode\":\"690\",\"ISOcode\":\"ISO 3166-2:SC\",\"Independent\":\"Yes\"},{\"Name\":\"Sierra Leone\",\"A2Code\":\"SL\",\"A3Code\":\"SLE\",\"NumCode\":\"694\",\"ISOcode\":\"ISO 3166-2:SL\",\"Independent\":\"Yes\"},{\"Name\":\"Singapore\",\"A2Code\":\"SG\",\"A3Code\":\"SGP\",\"NumCode\":\"702\",\"ISOcode\":\"ISO 3166-2:SG\",\"Independent\":\"Yes\"},{\"Name\":\"Slovakia\",\"A2Code\":\"SK\",\"A3Code\":\"SVK\",\"NumCode\":\"703\",\"ISOcode\":\"ISO 3166-2:SK\",\"Independent\":\"Yes\"},{\"Name\":\"Slovenia\",\"A2Code\":\"SI\",\"A3Code\":\"SVN\",\"NumCode\":\"705\",\"ISOcode\":\"ISO 3166-2:SI\",\"Independent\":\"Yes\"},{\"Name\":\"Solomon Islands\",\"A2Code\":\"SB\",\"A3Code\":\"SLB\",\"NumCode\":\"090\",\"ISOcode\":\"ISO 3166-2:SB\",\"Independent\":\"Yes\"},{\"Name\":\"Somalia\",\"A2Code\":\"SO\",\"A3Code\":\"SOM\",\"NumCode\":\"706\",\"ISOcode\":\"ISO 3166-2:SO\",\"Independent\":\"Yes\"},{\"Name\":\"South Africa\",\"A2Code\":\"ZA\",\"A3Code\":\"ZAF\",\"NumCode\":\"710\",\"ISOcode\":\"ISO 3166-2:ZA\",\"Independent\":\"Yes\"},{\"Name\":\"South Sudan\",\"A2Code\":\"SS\",\"A3Code\":\"SSD\",\"NumCode\":\"728\",\"ISOcode\":\"ISO 3166-2:SS\",\"Independent\":\"Yes\"},{\"Name\":\"Spain\",\"A2Code\":\"ES\",\"A3Code\":\"ESP\",\"NumCode\":\"724\",\"ISOcode\":\"ISO 3166-2:ES\",\"Independent\":\"Yes\"},{\"Name\":\"Sri Lanka\",\"A2Code\":\"LK\",\"A3Code\":\"LKA\",\"NumCode\":\"144\",\"ISOcode\":\"ISO 3166-2:LK\",\"Independent\":\"Yes\"},{\"Name\":\"Sudan\",\"A2Code\":\"SD\",\"A3Code\":\"SDN\",\"NumCode\":\"729\",\"ISOcode\":\"ISO 3166-2:SD\",\"Independent\":\"Yes\"},{\"Name\":\"Suriname\",\"A2Code\":\"SR\",\"A3Code\":\"SUR\",\"NumCode\":\"740\",\"ISOcode\":\"ISO 3166-2:SR\",\"Independent\":\"Yes\"},{\"Name\":\"Sweden\",\"A2Code\":\"SE\",\"A3Code\":\"SWE\",\"NumCode\":\"752\",\"ISOcode\":\"ISO 3166-2:SE\",\"Independent\":\"Yes\"},{\"Name\":\"Switzerland\",\"A2Code\":\"CH\",\"A3Code\":\"CHE\",\"NumCode\":\"756\",\"ISOcode\":\"ISO 3166-2:CH\",\"Independent\":\"Yes\"},{\"Name\":\"Syria\",\"A2Code\":\"SY\",\"A3Code\":\"SYR\",\"NumCode\":\"760\",\"ISOcode\":\"ISO 3166-2:SY\",\"Independent\":\"Yes\"},{\"Name\":\"Tajikistan\",\"A2Code\":\"TJ\",\"A3Code\":\"TJK\",\"NumCode\":\"762\",\"ISOcode\":\"ISO 3166-2:TJ\",\"Independent\":\"Yes\"},{\"Name\":\"Tanzania\",\"A2Code\":\"TZ\",\"A3Code\":\"TZA\",\"NumCode\":\"834\",\"ISOcode\":\"ISO 3166-2:TZ\",\"Independent\":\"Yes\"},{\"Name\":\"Thailand\",\"A2Code\":\"TH\",\"A3Code\":\"THA\",\"NumCode\":\"764\",\"ISOcode\":\"ISO 3166-2:TH\",\"Independent\":\"Yes\"},{\"Name\":\"East Timor-Leste\",\"A2Code\":\"TL\",\"A3Code\":\"TLS\",\"NumCode\":\"626\",\"ISOcode\":\"ISO 3166-2:TL\",\"Independent\":\"Yes\"},{\"Name\":\"Togo\",\"A2Code\":\"TG\",\"A3Code\":\"TGO\",\"NumCode\":\"768\",\"ISOcode\":\"ISO 3166-2:TG\",\"Independent\":\"Yes\"},{\"Name\":\"Tonga\",\"A2Code\":\"TO\",\"A3Code\":\"TON\",\"NumCode\":\"776\",\"ISOcode\":\"ISO 3166-2:TO\",\"Independent\":\"Yes\"},{\"Name\":\"Trinidad and Tobago\",\"A2Code\":\"TT\",\"A3Code\":\"TTO\",\"NumCode\":\"780\",\"ISOcode\":\"ISO 3166-2:TT\",\"Independent\":\"Yes\"},{\"Name\":\"Tunisia\",\"A2Code\":\"TN\",\"A3Code\":\"TUN\",\"NumCode\":\"788\",\"ISOcode\":\"ISO 3166-2:TN\",\"Independent\":\"Yes\"},{\"Name\":\"Turkey\",\"A2Code\":\"TR\",\"A3Code\":\"TUR\",\"NumCode\":\"792\",\"ISOcode\":\"ISO 3166-2:TR\",\"Independent\":\"Yes\"},{\"Name\":\"Turkmenistan\",\"A2Code\":\"TM\",\"A3Code\":\"TKM\",\"NumCode\":\"795\",\"ISOcode\":\"ISO 3166-2:TM\",\"Independent\":\"Yes\"},{\"Name\":\"Tuvalu\",\"A2Code\":\"TV\",\"A3Code\":\"TUV\",\"NumCode\":\"798\",\"ISOcode\":\"ISO 3166-2:TV\",\"Independent\":\"Yes\"},{\"Name\":\"Uganda\",\"A2Code\":\"UG\",\"A3Code\":\"UGA\",\"NumCode\":\"800\",\"ISOcode\":\"ISO 3166-2:UG\",\"Independent\":\"Yes\"},{\"Name\":\"Ukraine\",\"A2Code\":\"UA\",\"A3Code\":\"UKR\",\"NumCode\":\"804\",\"ISOcode\":\"ISO 3166-2:UA\",\"Independent\":\"Yes\"},{\"Name\":\"United Arab Emirates\",\"A2Code\":\"AE\",\"A3Code\":\"ARE\",\"NumCode\":\"784\",\"ISOcode\":\"ISO 3166-2:AE\",\"Independent\":\"Yes\"},{\"Name\":\"United Kingdom\",\"A2Code\":\"GB\",\"A3Code\":\"GBR\",\"NumCode\":\"826\",\"ISOcode\":\"ISO 3166-2:GB\",\"Independent\":\"Yes\"},{\"Name\":\"United States of America\",\"A2Code\":\"US\",\"A3Code\":\"USA\",\"NumCode\":\"840\",\"ISOcode\":\"ISO 3166-2:US\",\"Independent\":\"Yes\"},{\"Name\":\"Uruguay\",\"A2Code\":\"UY\",\"A3Code\":\"URY\",\"NumCode\":\"858\",\"ISOcode\":\"ISO 3166-2:UY\",\"Independent\":\"Yes\"},{\"Name\":\"Uzbekistan\",\"A2Code\":\"UZ\",\"A3Code\":\"UZB\",\"NumCode\":\"860\",\"ISOcode\":\"ISO 3166-2:UZ\",\"Independent\":\"Yes\"},{\"Name\":\"Vanuatu\",\"A2Code\":\"VU\",\"A3Code\":\"VUT\",\"NumCode\":\"548\",\"ISOcode\":\"ISO 3166-2:VU\",\"Independent\":\"Yes\"},{\"Name\":\"Venezuela\",\"A2Code\":\"VE\",\"A3Code\":\"VEN\",\"NumCode\":\"862\",\"ISOcode\":\"ISO 3166-2:VE\",\"Independent\":\"Yes\"},{\"Name\":\"Vietnam\",\"A2Code\":\"VN\",\"A3Code\":\"VNM\",\"NumCode\":\"704\",\"ISOcode\":\"ISO 3166-2:VN\",\"Independent\":\"Yes\"},{\"Name\":\"Yemen\",\"A2Code\":\"YE\",\"A3Code\":\"YEM\",\"NumCode\":\"887\",\"ISOcode\":\"ISO 3166-2:YE\",\"Independent\":\"Yes\"},{\"Name\":\"Zambia\",\"A2Code\":\"ZM\",\"A3Code\":\"ZMB\",\"NumCode\":\"894\",\"ISOcode\":\"ISO 3166-2:ZM\",\"Independent\":\"Yes\"},{\"Name\":\"Zimbabwe\",\"A2Code\":\"ZW\",\"A3Code\":\"ZWE\",\"NumCode\":\"716\",\"ISOcode\":\"ISO 3166-2:ZW\",\"Independent\":\"Yes\"}]}";
    private CountryList countries;

    public GameObject active;
    public GameObject menu;
    public GameObject over;
    public GameObject question;
    public GameObject staged;
    public GameObject a2;
    public GameObject a4;
    public GameObject a6;
    public GameObject a9;

    public AudioSource idle;
    public AudioSource playing;
    public AudioSource right;
    public AudioSource fail;
    public AudioSource win;

    private readonly System.Random r = new System.Random();
    public int upperbound = 100;
    public int stage = 0;
    private int time = -1;
    private int wait = -1;
    private int state = 0; // 0 = wrong, 1 = right, 2 = win
    private bool animating = false;
    private int answer = -1;
    private int[] stagesetting = new int[] { 2, 2, 4, 4, 6, 6, 9, 9 };

    private BannerView bannerView;
    private int[] answers;

    // Use this for initialization
    void Start()
    {
#if UNITY_ANDROID
        string appId = "ca-app-pub-7354315263788743~2145952587";
#elif UNITY_IPHONE
        string appId = "ca-app-pub-3940256099942544~1458002511";
#else
        string appId = "unexpected_platform";
#endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);
        countries = JsonUtility.FromJson<CountryList>(countryjson);
        upperbound = countries.List.Count;
        active = menu;
        idle.Play();
        RequestBanner();
    }

    private void RequestBanner()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-7354315263788743/4583757126";
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-3940256099942544/2934735716";
#else
            string adUnitId = "unexpected_platform";
#endif

        bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        AdRequest request = new AdRequest.Builder().Build();

        // Called when an ad request has successfully loaded.
        bannerView.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        bannerView.LoadAd(request);
    }

    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        //menu.GetComponentsInChildren<Text>()[1].text = "Banner loaded ";
        // Handle the ad failed to load event.
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        //menu.GetComponentsInChildren<Text>()[1].text = "Banner failed to load: " + args.Message;
        // Handle the ad failed to load event.
    }

    // Update is called once per frame
    void Update () {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) animating = false;
        if (stage == 0) //main menu
        {
            if (!animating && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                var touchDelta = Input.GetTouch(0).deltaPosition;
                if (touchDelta.y > 5)
                { //flick up
                    animating = true;
                    stage = 10; // 1-1
                    PrepStage();
                    return;
                }
            }
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            animating = true;
            active.GetComponent<Animation>().Play("SlideUp");
            menu.GetComponent<Animation>().Play("SlideIn");
            active = menu;
            stage = 0;
            time = 0;
            idle.Play();
            playing.Stop();
        }

        if (wait > 0)
        {
            wait--;
            if (wait == 0)
            {
                if (state == 0)
                {
                    GameOver("Wrong answer on stage " + (stage / 10) + "-" + ((stage % 10) + 1));
                    return;
                }
                if (state == 1)
                {
                    stage++;
                    if (stage % 10 == 0)
                    {
                        PrepStage();
                        return;
                    }
                    time = 180 + 120 * ((stage / 10) % 2) + 60;

                    GenerateQuestion();
                    question.GetComponentsInChildren<Text>()[0].text = (stage / 10) + "-" + ((stage % 10) + 1);
                    question.GetComponentsInChildren<Text>()[1].text = "5";

                    active.GetComponent<Animation>().Play("SlideUp");
                    question.GetComponent<Animation>().Play("SlideIn");
                    active = question;
                    return;
                }
                if (state == 2)
                {
                    GameOver("You Won!");
                    return;
                }
                GameOver("Error!");
            }
            return;
        }
        if (active == staged)
        {
            if (!animating && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                var touchDelta = Input.GetTouch(0).deltaPosition;
                if (touchDelta.y > 5)
                { //flick up
                    animating = true;
                    StartStage();
                    return;
                }
            }
            return;
        }

        if (stage > 0)
        {
            time--;
            if (active == question)
            {
                if (time == 0)
                {
                    GenerateAnswer();
                    return;
                }
                if (!animating && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    var touchDelta = Input.GetTouch(0).deltaPosition;
                    if (touchDelta.y > 5)
                    { //flick up
                        animating = true;
                        GenerateAnswer();
                        return;
                    }
                }
            }
            else
            {
                if (time == 0)
                {
                    playing.Stop();
                    fail.Play();
                    GameOver("Timeout on stage " + (stage / 10) + "-" + ((stage % 10) + 1));
                    return;
                }
            }
            active.GetComponentsInChildren<Text>()[1].text = (time / 60).ToString();
            return;
        }

        if (stage < 0)
        {
            if (!animating && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                var touchDelta = Input.GetTouch(0).deltaPosition;
                if (touchDelta.y > 5)
                { //flick up
                    animating = true;
                    over.GetComponent<Animation>().Play("SlideUp");
                    menu.GetComponent<Animation>().Play("SlideIn");
                    active = menu;
                    stage = 0;
                    idle.Play();
                    fail.Stop();
                    win.Stop();
                    return;
                }
            }
        }
	}

    private void GameOver(string v)
    {
        stage = -1;

        over.GetComponentsInChildren<Text>()[0].text = v;

        animating = true;
        active.GetComponent<Animation>().Play("SlideUp");
        over.GetComponent<Animation>().Play("SlideIn");
        active = over;
    }

    public void CheckAnswer(int answer)
    {
        if (wait > 0)
        {
            return;
        }
        var buttons = active.GetComponentsInChildren<Button>();
        var country = countries.List[this.answer];
        for (int i = 0; i < buttons.Length; i++)
        {
            if (countries.List[answers[i]].Name == country.Name)
            {
                var colors = buttons[i].GetComponentsInChildren<Image>()[0];
                colors.color = Color.green;
            } else
            {
                var colors = buttons[i].GetComponentsInChildren<Image>()[0];
                colors.color = Color.red;
            }
            buttons[i].GetComponentsInChildren<Text>()[0].text = countries.List[answers[i]].Name;
        }
        if (answer != this.answer)
        {
            playing.Stop();
            fail.Play();
            wait = 20;
            state = 0;
            return;
        }
        wait = 20;
        state = 1;
        if (stage + 1 == (stagesetting.Length + 1) * 10)
        {
            playing.Stop();
            win.Play();
            state = 2;
        }
        else if ((stage + 1) % 10 == 0)
        {
            win.Play();
        }
        else
        {
            right.Play();
        }
    }

    private void PrepStage()
    {
        if (stage == 10)
        {
            idle.Stop();
            playing.Play();

            staged.GetComponentsInChildren<Text>()[0].text = "Stage " + stage / 10;
        } else
        {
            staged.GetComponentsInChildren<Text>()[0].text = "Congratulation! You beat Stage " + ((stage / 10) - 1)+ "\r\nGet ready for Stage " + stage / 10;
            playing.Stop();
            win.Play();
        }

        animating = true;

        active.GetComponent<Animation>().Play("SlideUp");
        staged.GetComponent<Animation>().Play("SlideIn");
        active = staged;
    }

    private void StartStage()
    {
        if (stage != 10)
        {
            playing.Play();
        }
        animating = true;
        time = 180 + 120 * ((stage / 10) % 2) + 60;

        GenerateQuestion();
        question.GetComponentsInChildren<Text>()[0].text = (stage / 10) + "-" + ((stage % 10) + 1);
        question.GetComponentsInChildren<Text>()[1].text = "5";

        active.GetComponent<Animation>().Play("SlideUp");
        question.GetComponent<Animation>().Play("SlideIn");
        active = question;
    }

    private void GenerateQuestion()
    {
        answer = r.Next(upperbound);
        question.GetComponentsInChildren<Text>()[2].text = countries.List[answer].Name;
    }

    private void GenerateAnswer()
    {
        answers = new int[stagesetting[(stage-10) / 10]];
        answers[0] = answer;
        for (int i = 1; i < answers.Length;)
        {
            answers[i] = r.Next(upperbound);
            //make extra sure that there's no duplicate
            for (int j = 0; j < i; j++)
            {
                if (answers[j] == answers[i])
                {
                    i--;
                    j = i;
                }
            }
            i++;
        }
        Shuffle(answers);
        switch (answers.Length)
        {
            case 2:
                active = a2;
                break;
            case 4:
                active = a4;
                break;
            case 6:
                active = a6;
                break;
            case 9:
                active = a9;
                break;
            default:
                break;
        }
        active.GetComponentsInChildren<Text>()[0].text = (stage/10)+"-"+((stage%10)+1);
        for (int i = 0; i < answers.Length; i++)
        {
            var country = countries.List[answers[i]];
            var colors = active.GetComponentsInChildren<Button>()[i].GetComponentInChildren<Image>();
            var texture = Resources.Load(country.A2Code.ToLower()) as Texture2D;
            colors.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            colors.color = Color.white;
            active.GetComponentsInChildren<Button>()[i].onClick.RemoveAllListeners();
            var ani = answers[i];
            active.GetComponentsInChildren<Button>()[i].onClick.AddListener(() => {  if (!animating) CheckAnswer(ani); });
            active.GetComponentsInChildren<Button>()[i].GetComponentsInChildren<Text>()[0].text = "";
            active.GetComponentsInChildren<Button>()[i].GetComponentsInChildren<Text>()[0].color = Color.white;
        }
        time = 180 + 120 * ((stage/10)%2) + 60;
        question.GetComponent<Animation>().Play("SlideUp");
        active.GetComponent<Animation>().Play("SlideIn");
    }
    
    private void Shuffle<T>(T[] array)
    {
        int n = array.Length;
        while (n > 1)
        {
            int k = r.Next(n--);
            T temp = array[n];
            array[n] = array[k];
            array[k] = temp;
        }
    }
}
